---
- name: Install django prerequisites
  apt:
    state: "present"
    pkg:
      - git
      - build-essential
      - python3-virtualenv
      - "{{django_python_version}}"
      - "{{django_python_version}}-dev"
      - "{{django_pip_version}}"
      - memcached
      - libmemcached-dev
      # pillow build deps
      - libtiff5-dev
      - libjpeg8-dev
      - zlib1g-dev
      - libfreetype6-dev
      - liblcms2-dev
      - libwebp-dev
      - tcl8.6-dev
      - tk8.6-dev
      # trgm extension
      - "{{postgresql_contrib_name}}"
  tags:
   - django
   - django-apt

- name: Copy ssh keys
  template: >
    src={{item.template}}
    dest=/home/{{main_user_name}}/.ssh/{{item.name}}
    owner={{main_user_name}}
    group={{main_user_name}}
    mode=0600
  when: django_id_rsa_private is defined
  with_items:
   - template: django_id_rsa
     name: "{{django_project_name}}_id_rsa"
   - template: django_id_rsa.pub
     name: "{{django_project_name}}_id_rsa.pub"
  tags:
   - django
   - django-ssh-keys

- name: Copy django scripts
  git: >
    repo={{django_repo_url}}
    dest={{django_repo_dir}}
    version={{django_repo_version}}
    recursive={{django_git_recursive}}
    ssh_opts="-i /home/{{main_user_name}}/.ssh/{{django_project_name}}_id_rsa"
    accept_hostkey=True
  register: django_scripts_result
  ignore_errors: "{{ansible_check_mode}}"
  notify:
   - restart {{django_init_name}}
   - restart {{django_init_name}}-interface
   - restart {{django_init_name}}-celery
   - restart {{django_init_name}}-celery-beat

  tags:
   - django
   - djangocode

- name: Update submodules
  command: git submodule update
  args:
    chdir: "{{django_repo_dir}}"
  when: django_scripts_result.changed|bool
  tags:
   - django
   - djangocode

- name: Create django virtualenv
  command: virtualenv {{django_venv_dir}} -p {{django_python_version}} creates={{django_venv_dir}}
  tags:
   - django
   - django-venv

- name: Permissions for static cache and media
  when: django_perms|bool
  treeperms:
    paths: "{{django_project_file_permissions}}"
  tags:
    - django
    - djangoperms
    - djangocode

- name: Install django python dependencies
  pip: >
    requirements={{django_requirements_file}}
    virtualenv={{django_venv_dir}}
  notify:
   - restart {{django_init_name}}
  tags:
   - django
   - django-venv

- name: Install psycopg2
  pip: name=psycopg2 virtualenv={{django_venv_dir}}
  notify:
   - restart {{django_init_name}}
  tags:
   - django
   - django-venv

- name: Install django node dependencies
  block:
    - name: Check for package.json
      stat: path="{{django_project_dir}}package.json"
      register: package_json

    - name: Install yarn
      block:
        - apt_key:
            url: "https://dl.yarnpkg.com/debian/pubkey.gpg"
            id: 72ECF46A56B4AD39C907BBB71646B01B86E50310
        - apt_repository:
            repo: "deb https://dl.yarnpkg.com/debian/ stable main"
        - apt:
            name: yarn
      when: django_yarn

    - name: Install django node dependencies
      block:
        - name: with npm
          npm: path={{django_project_dir}}
          when: django_npm

        - name: Install django node dependencies with yarn
          yarn: path={{django_project_dir}}
          when:
            - django_yarn
            - not ansible_check_mode

      become: yes
      become_user: "{{main_user_name}}"
      when: package_json.stat.exists|bool

  tags:
   - django
   - django-node


- name: Install uwsgi
  pip: >
    name=uwsgi
    virtualenv={{django_venv_dir}}
  when: django_uwsgi|bool
  tags:
   - django
   - django-venv

- name: Copy django settings
  template: >
    src={{django_settings_template}}
    dest={{django_project_dir}}{{django_project_name}}/settings.py
  ignore_errors: "{{ansible_check_mode}}"
  notify:
   - restart {{django_init_name}}
  tags:
   - django
   - djangocode
   - django-settings
   - django-config

- name: Create django postgres role
  vars:
    ansible_ssh_pipelining: yes
  become: yes
  become_user: postgres
  postgresql_user: >
    name={{django_postgres_user}}
    password={{django_postgres_password}}
  ignore_errors: "{{ansible_check_mode}}"
  tags:
   - django
   - django-postgres

- name: Create django postgres database
  vars:
    ansible_ssh_pipelining: yes
  become: yes
  become_user: postgres
  postgresql_db: >
    name={{django_postgres_db}}
    owner={{django_postgres_user}}
  ignore_errors: "{{ansible_check_mode}}"
  tags:
   - django
   - django-postgres

- name: Install pg_trgm extension
  vars:
    ansible_ssh_pipelining: yes
  become: yes
  become_user: postgres
  postgresql_ext: name=pg_trgm db={{django_postgres_db}}
  ignore_errors: "{{ansible_check_mode}}"
  tags:
   - django
   - django-postgres

- name: Ensure logs dir exists
  file: >
    path={{django_logs_dir}}
    state=directory
    owner="www-data"
    group="{{main_user_name}}"
    mode="u=rwX,g=rwXs,o=rX"
  tags: django

- name: Ensure static dir is writable
  file: >
    path={{django_static_dir}}
    state=directory
    owner="www-data"
    group="{{main_user_name}}"
    mode="u=rwX,g=rwXs,o=rX"
  when: not django_perms
  tags: django

- name: Collect static
  become: yes
  become_user: "{{main_user_name}}"
  register: collectstatic_output
  changed_when: 'not collectstatic_output.stdout.startswith("\n0 static files copied")'
  command: "{{django_venv_dir}}bin/python {{django_project_dir}}manage.py collectstatic --noinput"
  tags:
   - django
   - djangocode
   - django-collectstatic

- name: Migrate
  when: django_run_migrate|bool
  command: "{{django_venv_dir}}bin/python {{django_project_dir}}manage.py migrate"
  register: migrate_output
  changed_when: '"No migrations to apply." not in migrate_output.stdout'
  tags:
   - django
   - djangocode

- name: Copy django nginx config
  template: >
    src={{django_nginx_template}}
    dest=/etc/nginx/sites-available/{{django_init_name}}.conf
  when: django_nginx|bool
  notify: restart nginx
  tags:
   - django
   - django-nginx
   - django-config

- name: Enable django nginx config
  file: >
    src=/etc/nginx/sites-available/{{django_init_name}}.conf
    dest=/etc/nginx/sites-enabled/{{django_init_name}}.conf
    state=link
    force=true
  when: django_nginx|bool
  notify: restart nginx
  tags:
   - django
   - django-nginx
   - django-config

- name: Copy max file size nginx config
  template: >
    src=etc_nginx_conf.d_max_file_size.conf
    dest=/etc/nginx/conf.d/max_file_size.conf
  when: django_nginx|bool
  notify: restart nginx
  tags:
   - django
   - django-nginx
   - django-config

# Upstart
- name: Copy django upstart config
  template: >
    src=uwsgi_init_script.conf
    dest=/etc/init/{{django_init_name}}.conf
  notify: restart {{django_init_name}}
  when: django_uwsgi and django_upstart
  tags:
   - django
   - django-upstart
   - django-config

# Systemd
- block:
  - name: Create services dir
    file: path=/usr/lib/systemd/system state=directory

  # uwsgi
  - block:
    - name: Copy django systemd config
      template: >
        src=uwsgi.service
        dest=/usr/lib/systemd/system/{{django_init_name}}.service
      notify:
       - systemctl daemon-reload
       - restart {{django_init_name}}

    - name: Enable django systemd config
      command: systemctl enable {{django_init_name}}.service
      register: systemd_enable
      changed_when: systemd_enable.stdout.find("Created") != -1

    when: django_uwsgi|bool

  # asgi
  - block:
    - name: Copy django asgi worker server config
      template: >
        src={{django_asgi_worker_template}}
        dest=/usr/lib/systemd/system/{{django_init_name}}.service
      notify:
       - systemctl daemon-reload
       - restart {{django_init_name}}

    - name: Enable django asgi worker server
      command: systemctl enable {{django_init_name}}.service
      register: systemd_enable
      changed_when: systemd_enable.stdout.find("Created") != -1

    - name: Copy django asgi interface server config
      template: >
        src={{django_asgi_interface_template}}
        dest=/usr/lib/systemd/system/{{django_init_name}}-interface.service
      notify:
       - systemctl daemon-reload
       - restart {{django_init_name}}-interface

    - name: Enable django asgi interface server
      command: systemctl enable {{django_init_name}}-interface.service
      register: systemd_enable
      changed_when: systemd_enable.stdout.find("Created") != -1

    when: django_asgi|bool

  # celery worker
  - block:
    - name: Copy celery config
      template: >
        src={{django_celery_template}}
        dest=/usr/lib/systemd/system/{{django_init_name}}-celery.service
      notify:
       - systemctl daemon-reload
       - restart {{django_init_name}}-celery

    - name: Enable celery
      command: systemctl enable {{django_init_name}}-celery.service
      register: systemd_enable
      changed_when: systemd_enable.stdout.find("Created") != -1

    when: django_celery|bool

  # celery beat
  - block:
    - name: Copy celery beat config
      template: >
        src={{django_celery_beat_template}}
        dest=/usr/lib/systemd/system/{{django_init_name}}-celery-beat.service
      notify:
       - systemctl daemon-reload
       - restart {{django_init_name}}-celery-beat

    - name: Enable celery beat
      command: systemctl enable {{django_init_name}}-celery-beat.service
      register: systemd_enable
      changed_when: systemd_enable.stdout.find("Created") != -1

    when: django_celery_beat|bool

  tags:
   - django
   - django-systemd
   - django-config
  when: django_systemd|bool
