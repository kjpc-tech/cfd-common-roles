#!/usr/bin/python
import os
import stat
import errno
import grp
import pwd
import re
import json

try:
    import selinux
    HAVE_SELINUX=True
except ImportError:
    HAVE_SELINUX=False

DOCUMENTATION = '''
---
module: treeperms
short_description: Set permissions to trees of files
description:
  - Handles idempotent, recursive setting of file permissions where some
    particular sub-branch might have different permissions from the parent. For
    example, setting restrictive permissions to all files in a web application,
    except for the "logs" directory, which the webserver keeps write access to.
    Attributes are applied recusively to all children of the given paths not
    otherwise specified within the `paths` argument.  Attributes are not inherited
    (e.g. "/server/logs", if specified, doesn't inherit specified attributes
    for "/server/").
notes:
    - See also M(file)
author:
    - "Charlie DeTar"
options:
    paths:
     - "Array of attribute arguments for file paths. Each entry must have the
       C('path') argument, and may have any of C(group), C(mode), C(owner),
       C(selevel), C(serole), C(setype), C(seuser) arguments to set those
       attributes, as defined in the M(file) module.  C(state), if provided,
       must be one of "file" or "directory".  If C(state) is ommitted, it will
       default to the current state of the given path, or "file" if the path
       does not exist. C(setgid_directories), if 'yes', will append setgid
       ('g+s') mode to the current path only if the curent path is a directory
       (similar to the behavior of the symbolic mode 'X', but for the setgid
       bit rather than executable bit).
'''

EXAMPLES = '''
# Set permissions of a server to one thing, but its 'logs' directory to
# something else.  Within 'logs', also add setgid to any directories, but not
# files.
- action: treeperms
  args:
    paths:
     - path: /var/local/server
       state: directory
       mode: u+rwX,g=rwX,o=rX
       owner: root
       group: dev
     - path: /var/local/server/logs
       state: directory
       mode: u+rwX,g=rwX,o=rX
       setgid_directories: yes
       owner: www-data
       group: dev
'''

# Copied from `file` module.
def get_state(path):
    ''' Find out current state '''

    if os.path.lexists(path):
        if os.path.islink(path):
            return 'link'
        elif os.path.isdir(path):
            return 'directory'
        elif os.stat(path).st_nlink > 1:
            return 'hard'
        else:
            # could be many other things, but defaulting to file
            return 'file'

    return 'absent'

def reduce_path_args(paths):
    """
    Build a trie-like datastructure that collects the listed paths.

    For example, convert:
      - path: /var/local/server
        mode: u+rwX,g=rwX,o=rX
        owner: root
        group: dev
      - path: /var/local/server/logs
        mode: u+rwX,g=rwX,o=rX
        owner: www-data
        group: dev
    to:
    {
      # root
      'action': None,
      'children': {
        'var': {
          'path': '/var',
          'action': None,
          'children': {
            'local': {
              'path': '/var/local',
              'action': None,
              'children': {
                'server': {
                  'path': '/var/local/server',
                  'action': {'owner': 'root', ...},
                  'children': {
                    'logs': {
                      'path': '/var/local/server/logs',
                      'action': {'owner': 'www-data', ...},
                      'children': {}
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    """
    trie = {'path': '/', 'action': None, 'children': {}}
    for desc in paths:
        abspath = os.path.abspath(os.path.expanduser(desc['path']))
        cur = trie
        for part in abspath.strip('/').split('/'):
            if part not in cur['children']:
                cur['children'][part] = {
                    'path': os.path.join(cur['path'], part),
                    'action': None,
                    'children': {}
                }
            cur = cur['children'][part]
        cur['action'] = desc.copy()
        cur['action']['path'] = abspath
    return trie

def _is_overridden_by_child(cur, path):
    """
    Returns True if the file specified by `path` is a descendant of a child of
    `cur` which has an action specified.
    """
    rel = os.path.relpath(path, cur['path'])
    parts = rel.split("/")
    while parts and (parts[0] in cur['children']):
        part = parts.pop(0)
        cur = cur['children'][part]
        if cur['action']:
            return True
    return False

def is_setgid_directories(action):
    """
    Returns True if the given action requests setgid_directories; False
    otherwise.
    """
    return str(action.get('setgid_directories', '')).lower() in ('yes', 'true')

def apply_attrs_with_setgid_directory(module, action, change_set):
    """
    Apply fs attributes if different.  If `setgid_directories` is requested and
    the current path is a directory, add `g+s` to the mode.  Add the path to
    `change_set` if attributes have changed.
    """
    if is_setgid_directories(action):
        new_action = action.copy()
        del new_action['setgid_directories']
        if get_state(action['path']) == "directory":
            new_action['mode'] = ','.join([m for m in (action.get('mode'), 'g+s') if m])
            action = new_action
    if module.set_fs_attributes_if_different(action, False):
        change_set.add(action['path'])

def set_tree_attributes(module, trie, change_set):
    cur = trie
    if cur['action']:
        # Set attrs on the top level path
        apply_attrs_with_setgid_directory(module, cur['action'], change_set)

        # Set attrs on any children not otherwise specified
        for root, dirs, files in os.walk(cur['action']['path']):
            for fsobj in dirs + files:
                fsname = os.path.join(root, fsobj)
                if _is_overridden_by_child(cur, fsname):
                    continue
                new_action = cur['action'].copy()
                new_action['path'] = fsname
                apply_attrs_with_setgid_directory(module, new_action, change_set)

    for name, child in cur['children'].items():
        set_tree_attributes(module, child, change_set)
    return change_set

def main():
    module = AnsibleModule(
        argument_spec = dict(
            paths = dict(required=True, type="list")
        ),
        supports_check_mode=True,
    )
    paths = module.params['paths']
    if not isinstance(paths, (list, tuple)):
        module.fail_json(msg="paths must be an array; found %s: %s" % (type(paths), paths))

    file_arg_list = []
    for args in paths:
        file_args = module.load_file_common_arguments(args)
        if 'state' in args:
            file_args['state'] = args['state']
        if 'setgid_directories' in args:
            file_args['setgid_directories'] = args['setgid_directories']
        file_arg_list.append(file_args)

    change_set = set()

    for file_args in file_arg_list:
        prev_state = get_state(file_args['path'])
        if 'state' not in file_args:
            # If state isn't specified, use its current filesystem state or 'file'.
            if prev_state != 'absent':
                file_args['state'] = prev_state
            else:
                file_args['state'] = 'file'

        if file_args['state'] not in ('file', 'directory'):
            module.fail_json(
                msg="Only 'file' and 'directory' states supported. " \
                    "'{}' found for path {}.".format(file_args['state'], file_args['path']))

        if prev_state != 'absent' and prev_state != file_args['state']:
            module.fail_json(msg="'{}' already exists as a {}".format(
                file_args['path'], prev_state))

        if prev_state == 'absent' and file_args['state'] == 'directory':
            if module.check_mode:
                change_set.add(file_args['path'])
                module.exit_json(paths=list(change_set), changed=True)
            curpath = ''
            path = file_args['path']

            # Create the directory recursively if it doesn't exist yet.
            # Implementation copied from 'file' module.
            for dirname in path.strip('/').split('/'):
                curpath = '/'.join([curpath, dirname])
                # Remove leading slash if we're creating a relative path
                if not os.path.isabs(path):
                    curpath = curpath.lstrip('/')
                if not os.path.exists(curpath):
                    try:
                        os.mkdir(curpath)
                    except OSError as ex:
                        # Possibly something else created the dir since the os.path.exists
                        # check above. As long as it's a dir, we don't need to error out.
                        if not (ex.errno == errno.EEXIST and os.isdir(curpath)):
                            raise
                    tmp_file_args = file_args.copy()
                    tmp_file_args['path'] = curpath
                    apply_attrs_with_setgid_directory(module, tmp_file_args, change_set)

        if is_setgid_directories(file_args):
            mode = file_args.get('mode')
            if isinstance(mode, int) or re.match('\d+', mode):
                msg = "'setgid_directories' can only be used with symbolic " \
                      "modes (e.g. 'u=rwx,g=rwx,o=rx'). Found octal mode " \
                      "'{}' for path '{}'.".format(mode, file_args['path'])
                return module.fail_json(msg=msg)

            if file_args.get('state') != 'directory':
                msg = "'setgid_directories' can only be used on directories. " \
                      "Found on type '{}' with path '{}'.".format(
                              file_args['state'],
                              file_args['path'])
                return module.fail_json(msg=msg)

    trie = reduce_path_args(file_arg_list)
    set_tree_attributes(module, trie, change_set)
    changed_paths = list(change_set)
    changed_paths.sort()
    module.exit_json(
            changed_paths=", ".join(changed_paths),
            changed=bool(changed_paths))

from ansible.module_utils.basic import *
if __name__ == "__main__":
    main()
