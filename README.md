# Common roles for webservers

A set of common ansible roles I use frequently for webservers.

# Roles

## common

Secure defaults for an ubuntu server.  Sets up automatic updates, logwatch,
ufw, ntp, ssh (no root logins, only key-based auth), fail2ban, postfix for
outbound mail only, and establishes a basic sudo user account.

Required vars:
 - `domain`: Canonical domain name for server.
 - `admin_email`: Email address to receive logwatch emails, etc. root is aliased to this.
 - `main_user_name`: The username for the main (sudo) user account.
 - `main_user_salt`: Salt to use in crypt implementation for setting user password.  Password will be set to the value of `ansible_become_pass`.
 - `ansible_become_pass`: Password to set for main user account. Should be either stored in a secure vault or retrieved from the commandline when executing the playbook with `--ask-become-pass`.
 - `main_user_authorized_keys`: String content of the public SSH keys to include in `.ssh/authorized_keys`.

Optional vars:
 - `ntp_servers`: List of ntp servers to use.  Defaults to [0-3].pool.ntp.org.
 - `timezone`: System timezone to use. Defaults to `"US/Mountain"``
 - `friendly_networks`: List of networks to exclude from fail2ban. Defaults to empty list.
 - `simple_postfix`: Set to `false` to skip installing/configuring postfix.

## swap

Installs a swapfile, adding extra memory overhead for extraordinary moments.

Optional vars:
 - `swap_file_path`: default `"/swapfile"`
 - `swap_file_size_kb`: default 1024

## apache

Installs apache.  Sets server fully-qualified domain name, enables SSL name
based virtual hosting, and disables default site.

Required vars:
 - `domain`: Canonical domain name for server.
 - `admin_email`: Email address for apache ServerAdmin directive.

## nginx

Installs nginx, removes default site.

## letsencrypt

Installs acme-tiny client for letsencrypt, and configures cron to refresh certs
once per month.  NOTE: This role *must* be preceeded by one of `letsencrypt-apache` or
`letsencrypt-nginx`, to set up the serving of the `.well-known` challenges
directory. This role will fail unless something is serving the acme challenges
directory on port 80.  For example, the roles ordering in the playbook should look something like:

    roles:
      ...
      - letsencrypt-nginx
      - letsencrypt
      ...

Required vars:
 - `letsencrypt_account_key`: String contents of account key for use with
   letsencrypt. Generate with e.g. `openssl genrsa 4096 > account.key`.
 - `letsencrypt_domains`: List of domain names to register with letsencrypt.
   Each domain must (obviously) have DNS entries mapping it to this server.

Optional vars (see roles/letsencrypt/defaults/main.yml):
 - `letsencrypt_domain_key_dir`: Private directory into which to put domain keys.
 - `letsencrypt_challenges_dir`: .well-known directory into which to put challenges.
 - `letsencrypt_acme_tiny_dir`: Location to install acme-tiny.
 - `letsencrypt_signed_cert`: Full file and path for signed cert
 - `letsencrypt_intermediate_cert`: Full file and path for intermediate cert.
 - `letsencrypt_chain`: Full file path and name for combined cert chain.

## letsencrypt-apache

Apache hooks for letsencrypt.  Installs VirtualHost on port 80 which serves the
.well-known directory for acme challenges, and redirects all other requests on
handled domains to port 443. Adds `/etc/apache2/site-includes/letsencrypt_ssl.conf`
which can be included by any other virtualhost to set up appropriate SSL
configuration.

## letsencrypt-nginx

Nginx hooks for letsencrypt. Installs a server listening on port 80 which
serves the .well-known directory for acme challenges, and redirects all other
requests on handled domains to port 443. Adds
`/etc/nginx/includes/letsencrypt_ssl.conf` which can be included by other
server directives to set up SSL.

## monitored

Installs packages and opens ports for the server to be a target of nagios and
munin monitoring.  Installs nagios-nrpe-server, nagios-plugins, munin-node, and
munin-node-extra.  Configures nagios and ufw to allow connections from a single
monitoring server.

Required vars:

 - `monitoring_server_ip`: IP address of monitoring server.

## mysql

Installs mysql.  Adds a /root/.my.cnf enabling root logins.

## postgresql

Installs postgresql. Sets up `pg_hba.conf` to allow md5 auth (password-based
authentication) from local users.

## supervisor

Installs supervisord, superlance, and configures supervisor to email
`{{admin_email}}` on errors.

## django

Opinionated setup for django 1.8+ running with postgresql and python3, via
nginx and uwsgi, with the application installed from a git repository.  The setup
results in:

 - django project installed from git
 - virtualenv with python dependencies set up
 - postgres database and user set up
 - uwsgi, with service started via upstart
 - static and media directories served via nginx.

required variables:

 - `django_domain`: Domain name for django site.
 - `django_project_name`: Name of django project; should be same name as folder in which settings file resides.
 - `django_repo_url`: URL to git repo from which to fetch django site.
 - `django_repo_dir`: Directory into which to clone repo.
 - `django_project_dir`: Full path to directory (possibly either the same
   directory as or a subdirectory of `django_repo_dir`) in which to find the
   django project.  This should be the directory in which `manage.py` is found.
 - `django_venv_dir`: Directory to create virtualenv in.
 - `django_static_dir`: Directory into which static files are collected. Value
   of django's `STATIC_ROOT` setting.
 - `django_media_dir`: Directory for media files. Value of django's `MEDIA_ROOT` setting.
 - `django_id_rsa_private`: String value of private SSH key for retrieving git repo.
 - `django_id_rsa_public`: String value of public SSH key for retrieving git repo.
 - `django_wsgi_port`: Port to serve Django's uwsgi daemon on.
 - `django_postgres_db`: Name of postgres database for Django to use.
 - `django_postgres_user`: Name of postgres user for Django to use.
 - `django_postgres_password`: Password for Django's postgres user.
 - `django_settings_template`: Path in which to find the `settings.py` template for Django.
